package jp.alhinc.ikemoto_masaaki.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales{
	public static void main(String[] args) {

		if (args.length == 0) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<>();
		Map<String, String> branchSales = new HashMap<>();

		System.out.println("ここにあるファイルを開きます => " + args[0]);

		BufferedReader branchDefinitionFileBr = null;
		BufferedReader salesFileBr = null;

		try {

			//支店定義ファイルの読み込み
			File branchDefinitionFile = new File(args[0], "branch.lst");

			//支店定義ファイルの存在確認
			if (!branchDefinitionFile.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			branchDefinitionFileBr = new BufferedReader(new FileReader(branchDefinitionFile));


			String branchLine = null;
			String salesLine = null;

			while ((branchLine = branchDefinitionFileBr.readLine()) != null) {
				//","で区切って配列に追加
				String[] items = branchLine.split(",");

				//フォーマット通りになっているかチェック
				if (items.length != 2 || !(items[0].matches("\\d{3}"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				} else {
					//そのままMapに追加
					branchNames.put(items[0], items[1]);
				}
			}

			File[] filesTable = new File(args[0]).listFiles();

			for (int i = 0; i < filesTable.length; i++) {

				List<String> salesLst = new ArrayList<>();
				String fileName = filesTable[i].getName();

				//売上ファイルが条件に合致しているか確認
				if (fileName.matches("^[0-9]{8}\\.+rcd$")) {

					String fileName2 = filesTable[i + 1].getName();

					if (fileName2.matches("^[0-9]{8}\\.+rcd$")) {
						String[] fileNameSplit = fileName.split("\\.");
						String[] fileName2Split = fileName2.split("\\.");
						int a = Integer.parseInt(fileNameSplit[0]);
						int b = Integer.parseInt(fileName2Split[0]);

						//連番チェック
						if (b - a != 1) {
							System.out.println("売上ファイル名が連番になっていません");
							return;
						}

					}
					File salesFile = new File(args[0], fileName);
					salesFileBr = new BufferedReader(new FileReader(salesFile));

					//各行（支店コード、売上金額）を売上集計リストに入れる
					while ((salesLine = salesFileBr.readLine()) != null) {
						salesLst.add(salesLine);
					}

					if (!(salesLst.get(1).matches("^[0-9]*$"))) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}

					if (salesLst.size() != 2) {
						System.out.println("<" + fileName + ">" + "のフォーマットが不正です");
						return;
					}

					if (!(branchNames.containsKey(salesLst.get(0)))) {
						System.out.println("<" + fileName + ">" + "の支店コードが不正です");
						return;
					}

					//支店コードの重複確認
					if (branchSales.containsKey(salesLst.get(0))) {
						Long fileSale = Long.parseLong(salesLst.get(1));
						Long mapSale = Long.parseLong(branchSales.get(salesLst.get(0)));

						//支店コードが一致しているもの同士で足し算
						branchSales.replace(salesLst.get(0), String.valueOf(fileSale + mapSale));

					} else {
						//そのままMapに追加
						branchSales.put(salesLst.get(0), salesLst.get(1));
					}

					//branchSalesの2番目の要素が10桁以下か確認
					if (branchSales.get(salesLst.get(0)).matches("^\\d{10,}$")) {
						System.out.println("合計金額が10桁を超えました");
					}
				}
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
		} finally {
			if (branchDefinitionFileBr != null) {
				try {
					branchDefinitionFileBr.close();

					if (salesFileBr != null) {
						salesFileBr.close();
					}
				} catch (IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}

		PrintWriter branchOutPw = null;

		try {

			File branchOut = new File(args[0], "branch.out");
			branchOutPw = new PrintWriter(branchOut);

			//売上集計ファイルに書き込み
			for (String s : branchSales.keySet()) {
				branchOutPw.println(s + "," + branchNames.get(s) + "," + branchSales.get(s));
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
		} finally {
			if (branchOutPw != null) {
				branchOutPw.close();
			}
		}
	}
}